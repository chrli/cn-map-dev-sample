import Vue from 'vue'
import App from './App.vue'
import { loadScript } from 'esri-loader'

Vue.config.productionTip = false

const options = {
  url: 'http://10.89.5.221/smiapi/arcgis/4.16/init.js',
};
loadScript(options)

new Vue({
  render: h => h(App),
}).$mount('#app')
