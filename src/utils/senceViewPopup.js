const senceViewPopup = {
  createContentpopup1(fields, data) {
    let htmlstring = '';
    htmlstring += "<table>"
    for (let key in fields) {
      htmlstring += "<tr>";
      htmlstring += '<td class="tdlabel">';
      htmlstring += "<span>";
      htmlstring += fields[key];
      htmlstring += " :";
      htmlstring += "</span>";
      htmlstring += "</td>";
      htmlstring += '<td class="tdvalue">';
      htmlstring += "<span>";
      htmlstring += data[key] != null ? data[key] : "";
      htmlstring += "</span>";
      htmlstring += "</td>";
      htmlstring += "</tr>";
    }
    htmlstring += "</table>"
    return htmlstring;
  },

  createContentpopup(data) {
    let htmlstring = '';
    htmlstring += "<table>"
    for (let key in data) {
      htmlstring += "<tr>";
      htmlstring += '<td class="tdlabel">';
      htmlstring += "<span>";
      htmlstring += key;
      htmlstring += " :";
      htmlstring += "</span>";
      htmlstring += "</td>";
      htmlstring += '<td class="tdvalue">';
      htmlstring += "<span>";
      htmlstring += data[key] != null ? data[key] : "";
      htmlstring += "</span>";
      htmlstring += "</td>";
      htmlstring += "</tr>";
    }
    htmlstring += "</table>"
    return htmlstring;
  },

  createCustompopup(data) {
    if (data.type === 'url') {
      var iframe = document.createElement('iframe')
      iframe.classList.add('popupframe')
      iframe.src = data.url
      return iframe
    } else if (data.type === 'html') {
      var div = document.createElement('div')
      div.innerHTML = data.html
      return div
    }
  },
}

export default senceViewPopup
