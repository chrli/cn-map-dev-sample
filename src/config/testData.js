const testParams = {
  pointsParameters: JSON.stringify({
    name: 'point2020',
    mode: 'add', //add delete replace
    dataArray: [{
        codX: 30000,
        codY: -31000,
        codZ: 10,
        attrs: {
          name: '一类(测试)',
          thewlevel: '一类',
          age: '20',
        },
      },
      {
        codX: 30000,
        codY: -30000,
        codZ: 10,
        attrs: {
          name: '二类',
          thewlevel: '二类',
          age: '20',
        },
      },
      {
        codX: 30000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '一类',
          thewlevel: '一类',
          age: '20',
        },
      },
      {
        codX: 29000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '二类',
          thewlevel: '二类',
          age: '20',
        },
      },
      {
        codX: 32000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '三类',
          thewlevel: '三类',
          age: '20',
        },
      },
      {
        codX: 33000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '二类',
          thewlevel: '二类',
          age: '20',
        },
      }
    ],
    popupEnabled: true,
    popupTemplate: {
      title: '{name}',
      content: [{
        type: 'fields',
        fieldInfos: [{
            fieldName: 'name',
            label: '名称',
          },
          {
            fieldName: 'thewlevel',
            label: '分类',
          },
          {
            fieldName: 'age',
            label: '年龄',
          },
        ],
      }, ],
    },
    legendVisible: false,
    type: 'point',
    fieldJsonArray: [{
        name: 'OBJECTID',
        alias: 'OBJECTID',
        type: 'oid',
      },
      {
        name: 'name',
        alias: '名称',
        type: 'string',
      },
      {
        name: 'thewlevel',
        alias: 'thewlevel',
        type: 'string',
      },
      {
        name: 'age',
        alias: '年龄',
        type: 'string',
      },
    ],
    renderer: {
      type: 'unique-value',
      field: 'thewlevel',
      defaultLabel: '无数据',
      defaultSymbol: {
        type: 'point-3d',
        symbolLayers: [{
          type: 'icon',
          size: 24,
          resource: {
            href: 'http://10.201.37.225:8080/images/pin_blue.png',
          },
        }, ],
      },
      uniqueValueInfos: [{
          value: '一类',
          label: 'Ⅰ类',
          symbol: {
            type: 'point-3d',
            symbolLayers: [{
              type: 'icon',
              size: 24,
              resource: {
                href: 'http://10.201.37.225:8080/images/pin_green.png',
              },
            }, ],
          },
        },
        {
          value: '二类',
          label: 'Ⅱ类',
          symbol: {
            type: 'point-3d',
            symbolLayers: [{
              type: 'icon',
              size: 24,
              resource: {
                href: 'http://10.201.37.225:8080/images/pin_red.png',
              },
            }, ],
          },
        },
        {
          value: '三类',
          label: 'Ⅲ类',
          symbol: {
            type: 'point-3d',
            symbolLayers: [{
              type: 'icon',
              size: 24,
              resource: {
                href: 'http://10.201.37.225:8080/images/pin_yellow.png',
              },
            }, ],
          },
        },
      ],
    },
    featureReduction: {
      type: 'selection',
    },
    // elevationInfo: {
    //   mode: 'on-the-ground'
    // },
    labelsymbol: {
      symbol: {
        type: 'text',
        color: 'black',
        haloSize: 1,
        haloColor: 'white',
      },
      labelPlacement: 'right-center',
      labelExpressionInfo:{
        expression: "$feature.name"
      },
    },
  }),
  pointsXhParameters: JSON.stringify({
    name: 'xhPoint',
    mode: 'add', //add delete replace
    dataArray: [{
        codX: 30000,
        codY: -31000,
        codZ: 10,
        attrs: {
          name: '一类(测试)',
          thewlevel: '一类',
          xh: '1',
        },
      },
      {
        codX: 30000,
        codY: -30000,
        codZ: 10,
        attrs: {
          name: '二类',
          thewlevel: '二类',
          xh: '2',
        },
      },
      {
        codX: 30000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '一类',
          thewlevel: '一类',
          xh: '3',
        },
      },
      {
        codX: 29000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '二类',
          thewlevel: '二类',
          xh: '4',
        },
      },
      {
        codX: 32000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '三类',
          thewlevel: '三类',
          xh: '5',
        },
      },
      {
        codX: 33000,
        codY: -29000,
        codZ: 10,
        attrs: {
          name: '二类',
          thewlevel: '二类',
          xh: '6',
        },
      }
    ],
    popupEnabled: true,
    popupTemplate: {
      title: '{name}',
      content: [{
        type: 'fields',
        fieldInfos: [{
            fieldName: 'name',
            label: '名称',
          },
          {
            fieldName: 'thewlevel',
            label: '分类',
          },
          {
            fieldName: 'xh',
            label: '序号',
          },
        ],
      }, ],
    },
    legendVisible: false,
    type: 'point',
    fieldJsonArray: [{
        name: 'OBJECTID',
        alias: 'OBJECTID',
        type: 'oid',
      },
      {
        name: 'name',
        alias: '名称',
        type: 'string',
      },
      {
        name: 'thewlevel',
        alias: 'thewlevel',
        type: 'string',
      },
      {
        name: 'xh',
        alias: '序号',
        type: 'string',
      },
    ],
    renderer: {
      type: 'unique-value',
      field: 'thewlevel',
      defaultLabel: '无数据',
      defaultSymbol: {
        type: 'point-3d',
        symbolLayers: [{
          type: 'icon',
          size: 24,
          resource: {
            href: 'http://10.201.37.225:8080/images/pin_blue.png',
          },
        }, ],
      },
      uniqueValueInfos: [{
          value: '一类',
          label: 'Ⅰ类',
          symbol: {
            type: 'point-3d',
            symbolLayers: [{
              type: 'icon',
              size: 24,
              resource: {
                href: 'http://10.201.37.225:8080/images/pin_green.png',
              },
            }, ],
          },
        },
        {
          value: '二类',
          label: 'Ⅱ类',
          symbol: {
            type: 'point-3d',
            symbolLayers: [{
              type: 'icon',
              size: 24,
              resource: {
                href: 'http://10.201.37.225:8080/images/pin_red.png',
              },
            }, ],
          },
        },
        {
          value: '三类',
          label: 'Ⅲ类',
          symbol: {
            type: 'point-3d',
            symbolLayers: [{
              type: 'icon',
              size: 24,
              resource: {
                href: 'http://10.201.37.225:8080/images/pin_yellow.png',
              },
            }, ],
          },
        },
      ],
    },
    featureReduction: {
      type: 'selection',
    },
    // elevationInfo: {
    //   mode: 'on-the-ground'
    // },
    labelsymbol: {
      symbol: {
        type: 'text',
        color: 'black',
        haloSize: 1,
        haloColor: 'white',
      },
      labelPlacement: 'center-center',
      labelExpressionInfo:{
        expression: "$feature.xh"
      },
    },
  }),
  polygonParameters: JSON.stringify({
    name: 'polygon2019',
    mode: 'add', //add delete replace
    dataArray: [{
        attrs: {
          name: '地块1',
          type: '地块1',
        },
        points: [{
            codX: 31000,
            codY: -31000,
            codZ: 0,
          },
          {
            codX: 31000,
            codY: -32000,
            codZ: 0,
          },
          {
            codX: 30000,
            codY: -32000,
            codZ: 0,
          },
          {
            codX: 30000,
            codY: -31000,
            codZ: 0,
          },
        ],
      },
      {
        attrs: {
          name: '地块2',
          type: '地块2',
        },
        points: [{
          codX: 33000,
          codY: -33000,
          codZ: 0,
        },
        {
          codX: 33000,
          codY: -34000,
          codZ: 0,
        },
        {
          codX: 32000,
          codY: -34000,
          codZ: 0,
        },
        {
          codX: 32000,
          codY: -33000,
          codZ: 0,
        },
        ],
      },
      {
        attrs: {
          name: '地块3',
          type: '地块3',
        },
        points: [{
          codX: 29000,
          codY: -31000,
          codZ: 0,
        },
        {
          codX: 29000,
          codY: -32000,
          codZ: 0,
        },
        {
          codX: 30000,
          codY: -32000,
          codZ: 0,
        },
        {
          codX: 30000,
          codY: -31000,
          codZ: 0,
        },
        ],
      },
    ],
    popupEnabled: true,
    popupTemplate: {
      title: '{name}',
      content: [{
        type: 'fields',
        fieldInfos: [{
            fieldName: 'name',
            label: '名称',
          },
          {
            fieldName: 'type',
            label: '类型',
          },
        ],
      }, ],
    },
    elevationInfo: {
      mode: 'on-the-ground'
    },
    legendVisible: true,
    type: 'polygon',
    fieldJsonArray: [{
        name: 'OBJECTID',
        alias: 'OBJECTID',
        type: 'oid',
      },
      {
        name: 'name',
        alias: '名称',
        type: 'string',
      },
      {
        name: 'type',
        alias: '类型',
        type: 'string',
      },
    ],
    renderer: {
      type: 'unique-value',
      field: 'type',
      defaultLabel: '其他',
      defaultSymbol: {
        type: 'simple-fill',
        color: [51, 51, 204, 0.9],
        style: 'solid',
        outline: {
          color: 'white',
          width: 1,
        },
      },
      uniqueValueInfos: [{
          value: '地块1',
          symbol: {
            type: 'simple-fill',
            color: [255, 0, 0, 0.9],
            style: 'solid',
            outline: {
              color: 'white',
              width: 1,
            },
          },
          label: '地块1',
        },
        {
          value: '地块2',
          symbol: {
            type: 'simple-fill',
            color: [0, 255, 0, 0.9],
            style: 'solid',
            outline: {
              color: 'white',
              width: 1,
            },
          },
          label: '地块2',
        },
        {
          value: '地块3',
          label: '地块3',
          symbol: {
            type: 'simple-fill',
            color: [0, 0, 255, 0.9],
            style: 'solid',
            outline: {
              color: 'white',
              width: 1,
            },
          },
        },
      ],
    },
    labelsymbol: {
      symbol: {
        type: 'text',
        color: 'black',
        haloSize: 2,
        haloColor: 'white',
      },
      labelPlacement: 'center-right',
    },
  }),
  popupIframeParameters: JSON.stringify({
    location: {
      codX: 30000,
      codY: -30000,
      codZ: 0,
    },
    title: 'test',
    content: {
      type: 'url',
      url: 'http://www.baidu.com',
    },
  }),
  popupHtmlParameters: JSON.stringify({
    location: {
      codX: 30000,
      codY: -30000,
      codZ: 0,
    },
    title: 'test',
    actions: [{
      title: '查看',
      id: 'map-search',
      className: 'esri-icon-search',
    }, ],
    content: {
      type: 'html',
      html: "<div><input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        "<input type='button' value='弹窗html' class='popupbg popupfg' />" +
        '</div>',
    },
  }),
}
export default testParams